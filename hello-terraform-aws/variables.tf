variable "aws_amis" {
  default = {
    us-east-2 = "ami-0fb653ca2d3203ac1"
  }
}

variable "aws_region" {
  description = "AWS Region to launch instances"
  default     = "us-east-2"
}