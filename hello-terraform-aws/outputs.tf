output "web_server_instance_id" {
  description = "ID of the EC2 web server instance"
  value       = aws_instance.web.id
}

output "web_server_instance_public_ip" {
  description = "IP of the EC2 web server instance"
  value       = aws_instance.web.public_ip
}

output "elb_public_dns_address" {
  description = "IP of the ELB"
  value       = aws_elb.iac-lab-webelb.dns_name
}
