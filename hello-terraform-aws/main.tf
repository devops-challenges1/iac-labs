terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
  required_version = ">= 0.14.9"
}

provider "aws" {
  shared_credentials_file = "~/.aws/credentials"
  profile                 = "administrator"
  region                  = var.aws_region
}

# create a VPC to launch our instance into it
resource "aws_vpc" "iac-lab-vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Owner = "djvruio@gmail.com"
	Name = "iac-lab-vpc"
  }
}

# create an internet Gateway to give our subnet access to the outside world
# https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Internet_Gateway.html
resource "aws_internet_gateway" "iac-lab-gw" {
  vpc_id = aws_vpc.iac-lab-vpc.id
}

# Grant the VPC internet access on its main route table
resource "aws_route" "internet-access" {
  route_table_id         = aws_vpc.iac-lab-vpc.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.iac-lab-gw.id
}

# create a subnet to launch our instance into
resource "aws_subnet" "iac-lab-subnet" {
  vpc_id                  = aws_vpc.iac-lab-vpc.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = true
}

# A security group for the ELB so it's accesible via web
resource "aws_security_group" "iac-lab-sg-elb" {
  name        = "iac-lab-sg-elb-example"
  description = "sg used in the terraform"
  vpc_id      = aws_vpc.iac-lab-vpc.id

  # http access from anywhere
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]

  }
}

resource "aws_security_group" "iac-lab-default" {
  name        = "iac-lab-default-sg"
  description = "used in terraform for http and ssh access"
  vpc_id      = aws_vpc.iac-lab-vpc.id

  #ssh access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #http access from the vpc
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_elb" "iac-lab-webelb" {
  subnets         = [aws_subnet.iac-lab-subnet.id]
  security_groups = [aws_security_group.iac-lab-sg-elb.id]
  instances       = [aws_instance.web.id]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }
}

resource "aws_instance" "web" {
  connection {
    type = "ssh"
    user = "ubuntu"
    host = self.public_ip
  }

  instance_type          = "t2.micro"
  ami                    = var.aws_amis[var.aws_region]
  vpc_security_group_ids = [aws_security_group.iac-lab-default.id]
  subnet_id              = aws_subnet.iac-lab-subnet.id

  user_data = <<-EOF
				#!/bin/bash
				sudo apt update -y
				sudo apt install apache2 -y
				sudo systemctl start apache2
				sudo bash -c 'echo our first web server > /var/www/html/index.html'
				EOF
  tags = {
    "Owner"     = "djvruio@gmail.com"
    Environment = "DEVEL"
  }
}
